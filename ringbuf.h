#include <stdbool.h>
#include <stdlib.h>

struct sub_buf {
    void* ptr;
    size_t len;
};

struct ringbuf {
    int read_pos;
    int store_pos;
    size_t num_elements;
    size_t buf_len;
    void* buf;
};

int ringbuf_init(struct ringbuf* rb, size_t len);
void ringbuf_free(struct ringbuf* rb);
size_t ringbuf_unused(struct ringbuf* rb);
struct sub_buf ringbuf_store_buf(struct ringbuf* rb);
void ringbuf_advance_store(struct ringbuf* rb, size_t n);
struct sub_buf ringbuf_read_buf(struct ringbuf* rb);
void ringbuf_advance_read(struct ringbuf* rb, size_t n);
