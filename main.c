#define _POSIX_C_SOURCE 199309L

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <unistd.h>

#include "ringbuf.h"

#define DEFAULT_SIZE 65536

int main(int argc, char** argv) {
    const int buf_size = argc > 1 ? atoi(argv[1]) : DEFAULT_SIZE;
    if (buf_size <= 0) {
        fprintf(stderr, "invalid buffer size: %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    // Make writes to stdout non-blocking
    int stdout_flags = fcntl(1, F_GETFL, 0);
    if (stdout_flags == -1) {
        fprintf(stderr, "unable to get stdout flags: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }
    stdout_flags |= O_NONBLOCK;
    int tmp = fcntl(1, F_SETFL, stdout_flags);
    if (tmp == -1) {
        fprintf(stderr, "unable to make stdout non-blocking: %s\n",
            strerror(errno));
        return EXIT_FAILURE;
    }

    struct ringbuf rb;
    tmp = ringbuf_init(&rb, buf_size);
    if (tmp == -1) {
        fprintf(stderr, "unable to init ring buffer: %s\n", strerror(errno));
        return EXIT_FAILURE;
    }

    int exit_code = EXIT_SUCCESS;
    bool eof = false;
    fd_set rfds;
    fd_set wfds;

    while (!eof || rb.num_elements > 0) {
        FD_ZERO(&rfds);
        FD_SET(0, &rfds); // stdin
        FD_ZERO(&wfds);
        FD_SET(1, &wfds); // stdout

        size_t unused = ringbuf_unused(&rb);
        bool rb_empty = unused == rb.buf_len;
        bool rb_full = unused == 0;

        int tmp = select(2, (rb_full || eof) ? NULL: &rfds,
            rb_empty ? NULL : &wfds, NULL, NULL);
        if (tmp == -1) {
            fprintf(stderr, "unable to wait for I/O: %s\n", strerror(errno));
            exit_code = EXIT_FAILURE;
            break;
        }

        if (!rb_full && !eof && FD_ISSET(0, &rfds)) {
            struct sub_buf store_buf = ringbuf_store_buf(&rb);
            ssize_t n = read(0, store_buf.ptr, store_buf.len);
            if (n == 0) {
                eof = true;
            } else if (n == -1) {
                fprintf(stderr, "unable to read: %s\n", strerror(errno));
                exit_code = EXIT_FAILURE;
                break;
            } else {
                ringbuf_advance_store(&rb, n);
            }
        }

        if (!rb_empty && FD_ISSET(1, &wfds)) {
            struct sub_buf read_buf = ringbuf_read_buf(&rb);
            ssize_t n = write(1, read_buf.ptr, read_buf.len);
            if (n == -1 && errno == EAGAIN) {
                // Not enough room to write
                // -> Do nothing
            } else if(n == -1) {
                fprintf(stderr, "unable to write: %s\n", strerror(errno));
                exit_code = EXIT_FAILURE;
                break;
            } else {
                ringbuf_advance_read(&rb, n);
            }
        }
    }

    ringbuf_free(&rb);
    return exit_code;
}
