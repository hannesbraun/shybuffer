# shybuffer

**shybuffer** is a buffer that forwards data from stdin to stdout. It may be
useful if your pipe's buffer is not large enough.

## Installation

Build requirements:
- C compiler
- make
- scdoc

To build and install **shybuffer**, run:

    make
    sudo make install

Passing PREFIX or DESTDIR is supported.
Uninstallation is also possible:

    sudo make uninstall

## Usage

See shybuffer(1).

## Contributing

Send patches/bug reports to [~hannes/public-inbox@lists.sr.ht][mailing-list]

## License

Licensed under the EUPL  
Copyright 2024 Hannes Braun

[mailing-list]: mailto:~hannes/public-inbox@lists.sr.ht
