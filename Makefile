.POSIX:

PREFIX?=/usr/local
BINDIR?=$(PREFIX)/bin
MANDIR?=$(PREFIX)/share/man
CFLAGS?=-O3

all: shybuffer shybuffer.1

shybuffer: main.c ringbuf.c ringbuf.h
	$(CC) -std=c99 -pedantic $(CFLAGS) -Wall -Wextra -Werror -Wno-unused-parameter -o shybuffer main.c ringbuf.c

shybuffer.1: shybuffer.1.scd
	scdoc < shybuffer.1.scd > shybuffer.1

clean:
	rm -rf shybuffer shybuffer.1

install: shybuffer shybuffer.1
	mkdir -m755 -p $(DESTDIR)$(BINDIR) $(DESTDIR)$(MANDIR)/man1
	install -m755 shybuffer $(DESTDIR)$(BINDIR)/shybuffer
	install -m644 shybuffer.1 $(DESTDIR)$(MANDIR)/man1/shybuffer.1

uninstall:
	rm -rf $(DESTDIR)$(BINDIR)/shybuffer
	rm -rf $(DESTDIR)$(MANDIR)/man1/shybuffer.1

.PHONY: all clean install uninstall
