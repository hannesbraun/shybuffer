#include "ringbuf.h"
#include <errno.h>
#include <stdint.h>

int ringbuf_init(struct ringbuf* rb, size_t len) {
    if (len < 1) {
        // Only buffer sizes bigger than 1 are allowed
        errno = EINVAL;
        return -1;
    }
    rb->read_pos = 0;
    rb->store_pos = 0;
    rb->num_elements = 0;
    rb->buf_len = len;
    rb->buf = malloc(len);
    if (rb->buf == NULL) {
        return -1;
    }
    return 0;
}

void ringbuf_free(struct ringbuf* rb) {
    free(rb->buf);
}

size_t ringbuf_unused(struct ringbuf* rb) {
    return rb->buf_len - rb->num_elements;
}

struct sub_buf ringbuf_store_buf(struct ringbuf* rb) {
    struct sub_buf store_buf;
    store_buf.ptr = (uint8_t*) rb->buf + rb->store_pos;
    store_buf.len = rb->buf_len - rb->store_pos;
    size_t unused = ringbuf_unused(rb);
    if (store_buf.len > unused) {
        store_buf.len = unused;
    }
    return store_buf;
}

void ringbuf_advance_store(struct ringbuf* rb, size_t n) {
    if (n > rb->buf_len - rb->num_elements) {
        n = rb->buf_len - rb->num_elements;
    }
    rb->store_pos = (rb->store_pos + n) % rb->buf_len;
    rb->num_elements += n;
}

struct sub_buf ringbuf_read_buf(struct ringbuf* rb) {
    struct sub_buf read_buf;
    read_buf.ptr = (uint8_t*) rb->buf + rb->read_pos;
    read_buf.len = rb->buf_len - rb->read_pos;
    if (read_buf.len > rb->num_elements) {
        read_buf.len = rb->num_elements;
    }
    return read_buf;
}

void ringbuf_advance_read(struct ringbuf* rb, size_t n) {
    if (n > rb->num_elements) {
        n = rb->num_elements;
    }
    rb->read_pos = (rb->read_pos + n) % rb->buf_len;
    rb->num_elements -= n;
}
